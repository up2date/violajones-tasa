#ifndef __CUDA_CIL_H__
#define __CUDA_CIL_H__

/*
 * This file makes sure that the necessary definitions of cuda types are
 * included in order to recognise keywords without the need to add support
 * within the compiler
 */


/* We include these for the definitions */
#include "driver_types.h"
#include "cuda_runtime_api.h"

/* The above cuda includes define these, so they are removed by the
 * preprocessor, so let's revert them back */
#define __device__ __device__
#define __host__ __host__
#define __global__ __global__

const struct uint3 blockIdx, blockDim, threadIdx ;

extern int atomicAdd(int *address, int val);
extern int atomicMax(int *address, int val);

#define CUDA_SAFE_CALL(x) x

/* We include also some commonly used functions */
extern "C" int printf(char const * __restrict __format , ...) ;
extern "C" void free(void *__ptr ) ;

#endif /*__CUDA_CIL_H__*/
