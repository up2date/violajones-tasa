#include "bsc_events.h"

void CreateMonitoringDescriptor();
void AddMonitors(int* pmc, int size);
void RemoveMonitors(int* pmc, int size);
void AddMonitor(int pmc);
void RemoveMonitor(int pmc);
void StartMonitoring();
void GetMonitorCount(int* size);
void ReadMonitors(long long int* values);
void StopMonitoring(long long int* values);
void ResetMonitors();
void ShutdownMonitoringDescriptor();
