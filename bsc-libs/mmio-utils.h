// API design requirements
// work with 64 bit fields
// read[0] is going to be always the timestamp

// maybe array of pointers is ideal given a known size


#define mmio_type long long int
#define NUM_ELEMENTS_SECTION 5
#define CYCBUFFER_SIZE 3

// Allocates n sections in a file. Removes any written content on the file
void MMIOMalloc(const char* filename, const unsigned int NumberOfSections);
// Truncates or extends the size of a file with a default value leaving te written content there
void MMIORealloc(const char* filename, const unsigned int NumberOfSections);
// Opens a file
void MMIOOpen(const char* filename);
// Writes in a file NUM_ELEMENTS_SECTION elements in the selected section
void MMIOWrite(const unsigned int section, long long int* value);
// Reads the cyclical buffer corresponding to a section
void MMIORead(const unsigned int section, long long int* value);
// Returns the size in bytes of the working file
unsigned int GetMemorySizeInBytes();
// Closes the currently opened file
void MMIOClose();