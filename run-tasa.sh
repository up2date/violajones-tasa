#!/bin/bash
i=0
rm results_cil.txt >/dev/null 2>&1
while [[ $i -le 9 ]]
do
	echo "--- Iteration #$i"
	rm violajones >/dev/null 2>&1
	rm violajones.i >/dev/null 2>&1
	rm violajones.cil.c >/dev/null 2>&1
	ciltutcc --gcc=nvcc -D__GNUC__ -D__CIL__ -U__cplusplus -D_Float128=__Float128 -D__null=0 --includedir=cuda_headers/ --enable-tut16 --save-temps -DUSING_PERF_WRAPPER -Ibsc-libs -c violajones.cu >/dev/null 2>&1
	sleep 1
	nvcc -g -x cu violajones.cil.c -L. -lm -lbscperf -lmmio-utils -lcuda -lcudart -lcupti -o violajones >/dev/null 2>&1
	sleep 1
	if [ -f "violajones" ]; then
		sudo ./violajones classifier.txt dataset/*.pgm >> results_cil.txt
		((i = i + 1))
	else
		echo "Error: Randomisation failed, repeating iteration..."
	fi
done
