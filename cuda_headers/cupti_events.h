/* When we are using TASA with nvcc, we want to use only C compliant features, but nvcc is actually using C++ linkage
 * so, we add extern "C" before we actully include the system header.
 */

extern "C" {

#include_next <cupti_events.h>

}
