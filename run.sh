#!/bin/bash
i=0
rm results.txt >/dev/null 2>&1
while [[ $i -le 9 ]]
do
	echo "--- Iteration #$i"
	rm violajones >/dev/null 2>&1
	nvcc -Ibsc-libs -DUSING_PERF_WRAPPER -g -x cu violajones.cu -L. -lm -lbscperf -lmmio-utils -lcuda -lcudart -lcupti -o violajones >/dev/null 2>&1
	sleep 1
	if [ -f "violajones" ]; then
		sudo ./violajones classifier.txt dataset/*.pgm >> results.txt
		((i = i + 1))
	else
		echo "Error: Randomisation failed, repeating iteration..."
	fi
done
