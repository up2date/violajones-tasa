sudo apt-get update
sudo apt-get install git -y
sudo apt-get install cmake -y
sudo apt-get install ocaml -y
sudo apt-get install camlp4 -y
sudo apt-get install ocamlbuild -y
sudo apt-get install perl -y

wget http://download.camlcity.org/download/findlib-1.9.1.tar.gz
tar -xvf findlib-1.9.1.tar.gz
cd findlib-1.9.1
./configure
make all opt
sudo make install
cd ..

git clone https://gitlab.bsc.es/lkosmidi/cil
cd cil
./configure
make
sudo make install
cd ..

wget http://ocamlgraph.lri.fr/download/ocamlgraph-1.8.8.tar.gz
tar -xvf ocamlgraph-1.8.8.tar.gz
cd ocamlgraph-1.8.8
./configure
make
sudo make install
sudo make install-findlib
cd ..

git clone https://gitlab.bsc.es/lkosmidi/tasa_cil.git
cd tasa_cil
mkdir build
cd build
cmake -DBUILD_TUT11=false -DBUILD_TUT15=false ..
make
sudo make install